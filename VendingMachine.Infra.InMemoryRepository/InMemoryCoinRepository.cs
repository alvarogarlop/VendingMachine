﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Application.Contracts.Repositories;
using VendingMachine.Domain.Models;

namespace VendingMachine.Infra.InMemoryRepository
{
    public class InMemoryCoinRepository
        : ICoinRepository
    {
        private readonly IEnumerable<Coin> _availableCoins;

        public InMemoryCoinRepository()
        {
            _availableCoins =
                new List<Coin>
                {
                    new Coin(0.01m, CoinType.OneCent),
                    new Coin(0.02m, CoinType.TwoCents),
                    new Coin(0.05m, CoinType.FiveCents),
                    new Coin(0.10m, CoinType.TenCents),
                    new Coin(0.20m, CoinType.TwentyCents),
                    new Coin(0.50m, CoinType.FiftyCents),
                    new Coin(1, CoinType.OneEuro),
                    new Coin(2, CoinType.TwoEuros)
                };
        }

        public Coin GetCoin(CoinType coinType)
        {
            var coin = _availableCoins.FirstOrDefault(x => x.Type.Equals(coinType));
            return coin;
        }

        public IEnumerable<Coin> GetAllCoins()
        {
            return _availableCoins;
        }
    }
}