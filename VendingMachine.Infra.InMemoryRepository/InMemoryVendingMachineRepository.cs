﻿using VendingMachine.Application.Contracts.Repositories;
using DomainVendingMachine = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Infra.InMemoryRepository
{
    public class InMemoryVendingMachineRepository
        : IVendingMachineRepository
    {
        private DomainVendingMachine _vendingMachine;

        public void Create(DomainVendingMachine vendingMachine)
        {
            _vendingMachine = vendingMachine;
        }

        public DomainVendingMachine Get()
        {
            return _vendingMachine;
        }

        public void Update(DomainVendingMachine vendingMachine)
        {
            _vendingMachine = vendingMachine;
        }
    }
}