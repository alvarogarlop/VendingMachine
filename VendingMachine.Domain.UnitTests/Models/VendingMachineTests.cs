﻿using FluentAssertions;
using Moq;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Exceptions;
using VendingMachine.Domain.Models;
using VendingMachine.Domain.States;
using VendingMachine.Domain.UnitTests.Builders;
using Xunit;
using VendingMachineModel = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Domain.UnitTests.Models
{
    public class VendingMachineTests
    {
        private readonly VendingMachineModel _vendingMachine;

        public VendingMachineTests()
        {
            _vendingMachine = 
                new VendingMachineBuilder()
                    .WithProduct(10, "Tea", 1.30m)
                    .WithProduct(20, "Espresso", 1.80m)
                    .WithProduct(20, "Juice", 1.80m)
                    .WithProduct(15, "Chicken soup", 1.80m)
                    .WithCoins(100, CoinType.TenCents)
                    .WithCoins(100, CoinType.TwentyCents)
                    .WithCoins(100, CoinType.FiftyCents)
                    .WithCoins(100, CoinType.OneEuro)
                    .Build();

            var noCreditState = new NoCreditState(_vendingMachine);
            _vendingMachine.UpdateState(noCreditState);
        }

        [Fact]
        public void Given_A_VendingMachine_With_NoCreditState_When_Inserting_A_Coin_Then_It_Should_Increment_The_Inserted_Amount()
        {
            var coinToInsert = new Coin(0.1m, CoinType.TenCents);
            _vendingMachine.InsertCoin(coinToInsert);

            _vendingMachine.InsertedAmount.Should().Be(0.10m);
        }

        [Fact]
        public void Given_A_VendingMachine_With_NoCreditState_When_Inserting_A_Coin_Then_The_Available_Coins_Should_Increase()
        {
            var coinToInsert = new Coin(0.1m, CoinType.TenCents);
            var availableCoinsCount = _vendingMachine.AvailableCoins.Count;
            _vendingMachine.InsertCoin(coinToInsert);

            _vendingMachine.AvailableCoins.Count.Should().Be(availableCoinsCount + 1);
        }

        [Fact]
        public void Given_A_VendingMachine_With_NoCreditState_When_Inserting_A_Coin_Then_The_Inserted_Coins_Should_Increase()
        {
            var coinToInsert = new Coin(0.1m, CoinType.TenCents);
            var insertedCoinsCount = _vendingMachine.InsertedCoins.Count;
            _vendingMachine.InsertCoin(coinToInsert);

            _vendingMachine.InsertedCoins.Count.Should().Be(insertedCoinsCount + 1);
        }

        [Fact]
        public void Given_A_VendingMachine_With_NoCreditState_When_Inserting_A_Coin_Then_The_State_Should_Be_CreditAvailableState()
        {
            var coinToInsert = new Coin(0.1m, CoinType.TenCents);
            _vendingMachine.InsertCoin(coinToInsert);

            _vendingMachine.State.GetType().Should().Be(typeof(CreditAvailableState));
        }

        [Fact]
        public void Given_A_VendingMachine_With_NoCreditState_When_Buying_A_Product_Then_It_Should_Throw_A_InsufficientAmountToBuyProductException()
        {
            InsufficientAmountToBuyProductException expectedException = null;
            var changeCalculator = Mock.Of<IChangeCalculatorDomainService>();
            var product = new Product("Tea", 1.3m);

            try
            {
                _vendingMachine.Buy(product, changeCalculator);
            }
            catch (InsufficientAmountToBuyProductException exception)
            {
                expectedException = exception;
            }

            expectedException.Should().NotBeNull();
        }

        [Fact]
        public void Given_A_VendingMachine_With_NoCreditState_When_Cancelling_The_Purchase_Then_It_Should_Return_No_Change()
        {
            var change = _vendingMachine.CancelPurchase();
            change.Should().BeEmpty();
        }
    }
}