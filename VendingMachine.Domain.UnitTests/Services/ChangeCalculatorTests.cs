﻿using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using VendingMachine.Domain.Models;
using VendingMachine.Domain.Services;
using VendingMachine.Domain.UnitTests.Builders;
using Xunit;

namespace VendingMachine.Domain.UnitTests.Services
{
    public class ChangeCalculatorTests
    {
        private readonly ChangeCalculator _sut;
        private decimal _paidAmount;
        private decimal _price;
        private List<Coin> _availableCoins;

        public ChangeCalculatorTests()
        {
            _sut = new ChangeCalculator();
        }

        [Fact]
        public void Given_A_Paid_Amount_Of_10_And_A_Price_Of_10_When_Calculating_The_Change_It_Should_Return_Empty_Change()
        {
            _paidAmount = 10m;
            _price = 10m;
            _availableCoins = new List<Coin>();

            var change = _sut.CalculateChange(_paidAmount, _price, _availableCoins);
            change.Should().BeEmpty();
        }

        [Fact]
        public void Given_A_Paid_Amount_Of_10_And_A_Price_Of_9_And_1_Coin_Of_1_Euro_Available_When_Calculating_The_Change_It_Should_Contain_1_Coin_Of_1_Euro()
        {
            _paidAmount = 10m;
            _price = 9m;
            _availableCoins =
                new List<Coin>
                {
                    new Coin(1, CoinType.OneEuro)
                };

            var change = _sut.CalculateChange(_paidAmount, _price, _availableCoins);
            change.Count.Should().Be(1);
            change.First().Type.Should().Be(CoinType.OneEuro);
        }

        [Fact]
        public void Given_A_Paid_Amount_Of_10_And_A_Price_Of_9_And_No_Coins_Of_1_Euro_Available_When_Calculating_The_Change_It_Should_Contain_2_Coins_Of_50_Cents()
        {
            _paidAmount = 10m;
            _price = 9m;
            _availableCoins =
                new CoinsBuilder()
                    .WithCoins(100, CoinType.FiftyCents)
                    .Build();

            var change = _sut.CalculateChange(_paidAmount, _price, _availableCoins);
            change.Count.Should().Be(2);
            change.Select(x => x.Type).Should().AllBeEquivalentTo(CoinType.FiftyCents);
        }

        [Fact]
        public void Given_A_Paid_Amount_Of_1_Point_46_And_A_Price_Of_1_Point_3_And_Infinite_Coins_Available_When_Calculating_The_Change_It_Should_Return_The_Minimal_Coins_Possible()
        {
            _paidAmount = 1.46m;
            _price = 1.3m;
            _availableCoins =
                new CoinsBuilder()
                    .WithCoins(100, CoinType.OneCent)
                    .WithCoins(100, CoinType.TwoEuros)
                    .WithCoins(100, CoinType.FiveCents)
                    .WithCoins(100, CoinType.TenCents)
                    .WithCoins(100, CoinType.TwentyCents)
                    .WithCoins(100, CoinType.FiftyCents)
                    .WithCoins(100, CoinType.OneEuro)
                    .WithCoins(100, CoinType.TwoEuros)
                    .Build();

            var change = _sut.CalculateChange(_paidAmount, _price, _availableCoins);
            change.Count.Should().Be(3);
            change.Count(x => x.Type.Equals(CoinType.TenCents)).Should().Be(1);
            change.Count(x => x.Type.Equals(CoinType.FiveCents)).Should().Be(1);
            change.Count(x => x.Type.Equals(CoinType.OneCent)).Should().Be(1);
        }
    }
}