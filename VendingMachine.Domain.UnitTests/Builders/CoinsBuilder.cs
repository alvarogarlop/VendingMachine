﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Domain.Models;

namespace VendingMachine.Domain.UnitTests.Builders
{
    public class CoinsBuilder
    {
        private readonly List<Coin> _coins;
        private readonly List<Coin> _availableCoins;

        public CoinsBuilder()
        {
            _coins = new List<Coin>();
            _availableCoins =
                new List<Coin>
                {
                    new Coin(0.01m, CoinType.OneCent),
                    new Coin(0.02m, CoinType.TwoCents),
                    new Coin(0.05m, CoinType.FiveCents),
                    new Coin(0.10m, CoinType.TenCents),
                    new Coin(0.20m, CoinType.TwentyCents),
                    new Coin(0.50m, CoinType.FiftyCents),
                    new Coin(1, CoinType.OneEuro),
                    new Coin(2, CoinType.TwoEuros)
                };
        }

        public CoinsBuilder WithCoins(int numberOfCoins, CoinType coinType)
        {
            var coin = _availableCoins.FirstOrDefault(x => x.Type.Equals(coinType));
            var coinsOfType = Enumerable.Repeat(coin, numberOfCoins);
            _coins.AddRange(coinsOfType);

            return this;
        }

        public List<Coin> Build()
        {
            return _coins;
        }
    }
}