﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Domain.Models;
using DomainVendingMachine = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Domain.UnitTests.Builders
{
    public class VendingMachineBuilder
    {
        private readonly List<Coin> _availableCoins;
        private readonly List<Product> _availableProducts;

        public VendingMachineBuilder()
        {
            _availableCoins = new List<Coin>();
            _availableProducts = new List<Product>();
        }

        public VendingMachineBuilder WithCoins(int numberOfCoins, CoinType coinType)
        {
            var coins =
                new CoinsBuilder()
                    .WithCoins(numberOfCoins, coinType)
                    .Build();
            _availableCoins.AddRange(coins);

            return this;
        }

        public VendingMachineBuilder WithProduct(int numberOfProducts, string name, decimal price)
        {
            var product = new Product(name, price);
            var products = Enumerable.Repeat(product, numberOfProducts);
            _availableProducts.AddRange(products);

            return this;
        }

        public DomainVendingMachine Build()
        {
            return new DomainVendingMachine(_availableCoins, _availableProducts);
        }
    }
}