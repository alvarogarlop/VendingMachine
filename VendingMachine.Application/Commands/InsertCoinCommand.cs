﻿using VendingMachine.Application.Contracts;
using VendingMachine.Domain.Models;

namespace VendingMachine.Application.Commands
{
    public class InsertCoinCommand 
        : ICommand
    {
        public Coin Coin { get; }
        public string Name { get; } = "InsertCoinCommand";

        public InsertCoinCommand(Coin coin) 
        {
            Coin = coin;
        }
    }
}