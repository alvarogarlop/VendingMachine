﻿using VendingMachine.Application.Contracts;

namespace VendingMachine.Application.Commands
{
    public class CancelPurchaseCommand
        : ICommand
    {
        public string Name { get; } = "CancelPurchaseCommand";
    }
}