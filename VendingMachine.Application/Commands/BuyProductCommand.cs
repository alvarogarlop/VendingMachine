﻿using VendingMachine.Application.Contracts;
using VendingMachine.Domain.Models;

namespace VendingMachine.Application.Commands
{
    public class BuyProductCommand
        : ICommand
    {
        public Product Product { get; }
        public string Name { get; } = "BuyProductCommand";

        public BuyProductCommand(Product product) 
        {
            Product = product;
        }
    }
}