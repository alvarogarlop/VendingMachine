﻿using System.Collections.Generic;
using VendingMachine.Application.Contracts;
using VendingMachine.Domain.Models;

namespace VendingMachine.Application.CommandResults
{
    public class BuyProductCommandResult
        : ICommandResult
    {
        public bool Ok { get; }
        public IEnumerable<Coin> Change { get; }
        public string Message { get; }

        public BuyProductCommandResult(bool ok, IEnumerable<Coin> change, string message = "")
        {
            Ok = ok;
            Change = change;
            Message = message;
        }
    }
}