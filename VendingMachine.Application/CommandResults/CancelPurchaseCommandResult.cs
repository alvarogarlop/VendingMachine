﻿using System.Collections.Generic;
using VendingMachine.Application.Contracts;
using VendingMachine.Domain.Models;

namespace VendingMachine.Application.CommandResults
{
    public class CancelPurchaseCommandResult
        : ICommandResult
    {
        public IEnumerable<Coin> Change { get; }

        public CancelPurchaseCommandResult(IEnumerable<Coin> change)
        {
            Change = change;
        }
    }
}