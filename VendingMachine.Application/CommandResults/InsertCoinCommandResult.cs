﻿using VendingMachine.Application.Contracts;

namespace VendingMachine.Application.CommandResults
{
    public class InsertCoinCommandResult
        : ICommandResult
    {
        public decimal DepositedAmount { get; }

        public InsertCoinCommandResult(decimal depositedAmount)
        {
            DepositedAmount = depositedAmount;
        }
    }
}
