﻿using System.Collections.Generic;
using VendingMachine.Domain.Models;

namespace VendingMachine.Application.Contracts.Repositories
{
    public interface ICoinRepository
    {
        Coin GetCoin(CoinType coinType);
        IEnumerable<Coin> GetAllCoins();
    }
}