﻿using DomainVendingMachine = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Application.Contracts.Repositories
{
    public interface IVendingMachineRepository
    {
        DomainVendingMachine Get();
        void Update(DomainVendingMachine vendingMachine);
    }
}