﻿namespace VendingMachine.Application.Contracts
{
    public interface ICommandHandler
    {
        string CommandName { get; }
        ICommandResult Handle(ICommand command);
    }
}