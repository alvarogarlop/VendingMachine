﻿namespace VendingMachine.Application.Contracts
{
    public interface ICommand
    {
        string Name { get; }
    }
}