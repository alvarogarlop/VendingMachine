﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Application.CommandResults;
using VendingMachine.Application.Commands;
using VendingMachine.Application.Contracts;
using VendingMachine.Application.Contracts.Repositories;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Exceptions;
using VendingMachine.Domain.Models;

namespace VendingMachine.Application.CommandHandlers
{
    public class BuyProductCommandHandler
        : ICommandHandler
    {
        public string CommandName { get; } = "BuyProductCommand";
        private readonly IVendingMachineRepository _vendingMachineRepository;
        private readonly IChangeCalculatorDomainService _changeCalculator;

        public BuyProductCommandHandler(IVendingMachineRepository vendingMachineRepository, IChangeCalculatorDomainService changeCalculator)
        {
            _vendingMachineRepository = vendingMachineRepository;
            _changeCalculator = changeCalculator;
        }


        public ICommandResult Handle(ICommand command)
        {
            var buyProductCommand = (BuyProductCommand)command;
            var vendingMachine = _vendingMachineRepository.Get();

            var ok = false;
            var change = new List<Coin>();
            string message;
            try
            {
                change = vendingMachine.Buy(buyProductCommand.Product, _changeCalculator).ToList();
                _vendingMachineRepository.Update(vendingMachine);

                ok = true;
                message = "Thank you";
            }
            catch (InsufficientAmountToBuyProductException)
            {
                message = "Insufficient Amount";
            }
            catch (ProductNotAvailableException)
            {
                message = "Product Not Available";
            }

            var buyProductCommandResult = new BuyProductCommandResult(ok, change, message);
            return buyProductCommandResult;
        }
    }
}