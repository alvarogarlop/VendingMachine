﻿using VendingMachine.Application.CommandResults;
using VendingMachine.Application.Commands;
using VendingMachine.Application.Contracts;
using VendingMachine.Application.Contracts.Repositories;

namespace VendingMachine.Application.CommandHandlers
{
    public class InsertCoinCommandHandler
        : ICommandHandler
    {
        public string CommandName { get; } = "InsertCoinCommand";
        private readonly IVendingMachineRepository _vendingMachineRepository;

        public InsertCoinCommandHandler(IVendingMachineRepository vendingMachineRepository)
        {
            _vendingMachineRepository = vendingMachineRepository;
        }

        public ICommandResult Handle(ICommand command)
        {
            var insertCoinCommand = (InsertCoinCommand) command;
            var vendingMachine = _vendingMachineRepository.Get();
            var amountInserted = vendingMachine.InsertCoin(insertCoinCommand.Coin);
            _vendingMachineRepository.Update(vendingMachine);

            var insertCoinCommandResult = new InsertCoinCommandResult(amountInserted);
            return insertCoinCommandResult;
        }
    }
}