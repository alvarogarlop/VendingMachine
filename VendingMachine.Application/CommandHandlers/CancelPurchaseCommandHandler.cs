﻿using VendingMachine.Application.CommandResults;
using VendingMachine.Application.Contracts;
using VendingMachine.Application.Contracts.Repositories;

namespace VendingMachine.Application.CommandHandlers
{
    public class CancelPurchaseCommandHandler
        : ICommandHandler
    {
        public string CommandName { get; } = "CancelPurchaseCommand";
        private readonly IVendingMachineRepository _vendingMachineRepository;

        public CancelPurchaseCommandHandler(IVendingMachineRepository vendingMachineRepository)
        {
            _vendingMachineRepository = vendingMachineRepository;
        }

        public ICommandResult Handle(ICommand command)
        {
            var vendingMachine = _vendingMachineRepository.Get();
            var change = vendingMachine.CancelPurchase();
            _vendingMachineRepository.Update(vendingMachine);

            var commandResult = new CancelPurchaseCommandResult(change);
            return commandResult;
        }
    }
}