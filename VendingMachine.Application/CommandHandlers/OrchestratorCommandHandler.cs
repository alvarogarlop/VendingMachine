﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Application.Contracts;

namespace VendingMachine.Application.CommandHandlers
{
    public class OrchestratorCommandHandler
        : ICommandHandler
    {
        public string CommandName { get; } = "OrchestratorCommand";
        private readonly IEnumerable<ICommandHandler> _commandHandlers;

        public OrchestratorCommandHandler(IEnumerable<ICommandHandler> commandHandlers)
        {
            _commandHandlers = commandHandlers;
        }

        public ICommandResult Handle(ICommand command)
        {
            var commandHandler = _commandHandlers.First(x => x.CommandName.Equals(command.Name));

            var commandResult = commandHandler.Handle(command);
            return commandResult;
        }
    }
}