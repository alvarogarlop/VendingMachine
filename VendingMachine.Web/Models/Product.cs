﻿namespace VendingMachine.Web.Models
{
    public class Product
    {
        public string Name { get; }
        public decimal Price { get; }
        public int Units { get; }

        public Product(string name, decimal price, int units)
        {
            Name = name;
            Price = price;
            Units = units;
        }
    }
}