﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace VendingMachine.Web.Models
{
    public class VendingMachineViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public string SelectedProductName { get; set; }
        public string SelectedProductPrice { get; set; }
        public string SelectedProductUnits { get; set; }
        public IEnumerable<SelectListItem> CoinOptions { get; set; }
        public Change Change { get; set; }
        public string Message { get; set; }
        public string SelectedCoin { get; set; }
        public decimal TotalAmount { get; set; }

        public VendingMachineViewModel()
        {
            Products = new List<Product>();
            CoinOptions = new List<SelectListItem>();
            Change = new Change();
            TotalAmount = 0.00m;
        }
    }
}