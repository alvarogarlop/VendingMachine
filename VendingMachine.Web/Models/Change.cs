﻿using System.Collections.Generic;
using System.Text;
using VendingMachine.Domain.Models;

namespace VendingMachine.Web.Models
{
    public class Change
    {
        public Dictionary<CoinType, int> Coins { get; set; }

        public Change()
        {
            Coins = new Dictionary<CoinType, int>();
        }

        public void AddCoin(CoinType coinType)
        {
            if (Coins.TryGetValue(coinType, out var units))
            {
                Coins[coinType] = units + 1;
            }
            else
            {
                Coins.Add(coinType, 1);
            }
        }

        public override string ToString()
        {
            var change = new StringBuilder();
            foreach (var coinType in Coins.Keys)
            {
                change.Append($"{Coins[coinType]} coins of {coinType.ToString()}, ");
            }
            return change.ToString();
        }
    }
}