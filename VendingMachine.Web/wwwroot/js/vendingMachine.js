﻿function insertCoin() {
    var insertedCoin = document.getElementById('Coins').value;
    $.ajax({
        url: '/Home/InsertCoin',
        type: 'POST',
        dataType: 'json',
        data: { insertedCoinType: insertedCoin },
        success: function (data) {
            document.getElementById('TotalAmount').innerText = data;
        },
        error: function() {
            alert('error');
        }
    });
}

function buyProduct(productName) {
    var productPrice = document.getElementById(productName + "_Price").innerText;
    var units = document.getElementById(productName + "_Units").innerText;
    $.ajax({
        url: '/Home/BuyProduct',
        type: 'POST',
        dataType: 'json',
        data:
        {
            productName: productName,
            productPrice: productPrice,
            units: units
        },
        success: function (data) {
            document.getElementById('Message').innerText = data.message;
            if (data.success)
            {
                document.getElementById('Change').innerText = data.change;
                document.getElementById(productName + "_Units").innerText = data.units;
                document.getElementById("TotalAmount").innerText = "0.00";
            }
        },
        error: function (data) {
        }
    });
}

function cancelPurchase() {
    $.ajax({
        url: '/Home/CancelPurchase',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            document.getElementById('Change').innerText = data;
            document.getElementById("TotalAmount").innerText = "0.00";
        },
        error: function () {
            alert('error');
        }
    });
}