﻿using Autofac;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Services;

namespace VendingMachine.Web.CompositionRoot
{
    public class DomainServicesModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ChangeCalculator>()
                .As<IChangeCalculatorDomainService>()
                .SingleInstance();
        }
    }
}