﻿using Autofac;
using Autofac.Core;
using System.Collections.Generic;
using VendingMachine.Application.CommandHandlers;
using VendingMachine.Application.Contracts;

namespace VendingMachine.Web.CompositionRoot
{
    public class CommandHandlerModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<InsertCoinCommandHandler>()
                .Keyed<ICommandHandler>("commandHandler");

            builder
                .RegisterType<BuyProductCommandHandler>()
                .Keyed<ICommandHandler>("commandHandler");

            builder
                .RegisterType<CancelPurchaseCommandHandler>()
                .Keyed<ICommandHandler>("commandHandler");

            builder
                .RegisterType<OrchestratorCommandHandler>()
                .As<ICommandHandler>()
                .WithParameter(
                    new ResolvedParameter(
                        (p, c) => p.Name.Equals("commandHandlers"),
                        (p, c) => c.ResolveKeyed<IEnumerable<ICommandHandler>>("commandHandler")
                    ));
        }
    }
}