﻿using Autofac;
using VendingMachine.Application.Contracts.Repositories;
using VendingMachine.Domain.Models;
using VendingMachine.Infra.InMemoryRepository;
using VendingMachine.Web.Builders;

namespace VendingMachine.Web.CompositionRoot
{
    public class RepositoryModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<InMemoryCoinRepository>()
                .As<ICoinRepository>()
                .SingleInstance();

            builder
                .Register(context =>
                {
                    var coinRepository = context.Resolve<ICoinRepository>();

                    var vendingMachine =
                        new VendingMachineBuilder(coinRepository)
                            .WithProduct(10, "Tea", 1.30m)
                            .WithProduct(20, "Espresso", 1.80m)
                            .WithProduct(20, "Juice", 1.80m)
                            .WithProduct(15, "Chicken soup", 1.80m)
                            .WithCoins(100, CoinType.TenCents)
                            .WithCoins(100, CoinType.TwentyCents)
                            .WithCoins(100, CoinType.FiftyCents)
                            .WithCoins(100, CoinType.OneEuro)
                            .Build();

                    var inMemoryRepository = new InMemoryVendingMachineRepository();
                    inMemoryRepository.Create(vendingMachine);
                    return inMemoryRepository;
                })
                .As<IVendingMachineRepository>()
                .SingleInstance();
        }
    }
}