﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using VendingMachine.Application.CommandResults;
using VendingMachine.Application.Commands;
using VendingMachine.Application.Contracts;
using VendingMachine.Application.Contracts.Repositories;
using VendingMachine.Domain.Models;
using VendingMachine.Web.Models;
using ProductDomain = VendingMachine.Domain.Models.Product;
using ProductViewModel = VendingMachine.Web.Models.Product;

namespace VendingMachine.Web.Controllers
{
    public class Home
        : Controller
    {
        private readonly ICoinRepository _coinRepository;
        private readonly IVendingMachineRepository _vendingMachineRepository;
        private readonly ICommandHandler _commandHandler;

        public Home(
            ICoinRepository coinRepository, 
            ICommandHandler commandHandler, 
            IVendingMachineRepository vendingMachineRepository)
        {
            _coinRepository = coinRepository;
            _commandHandler = commandHandler;
            _vendingMachineRepository = vendingMachineRepository;
        }

        public IActionResult Index()
        {
            var vendingMachine = _vendingMachineRepository.Get();

            var viewModel =
                new VendingMachineViewModel
                {
                    Products = MapProducts(vendingMachine.AvailableProducts),
                    CoinOptions =
                        new List<SelectListItem>
                        {
                            new SelectListItem("1 Cent", "OneCent"),
                            new SelectListItem("2 Cents", "TwoCents"),
                            new SelectListItem("5 Cents", "FiveCents"),
                            new SelectListItem("10 Cents", "TenCents"),
                            new SelectListItem("20 Cents", "TwentyCents"),
                            new SelectListItem("50 Cents", "FiftyCents"),
                            new SelectListItem("1 Euro", "OneEuro"),
                            new SelectListItem("2 Euro", "TwoEuros")
                        },
                    Change = new Change(),
                    Message = string.Empty
                };

            return View(viewModel);
        }

        public ActionResult InsertCoin(string insertedCoinType)
        {
            Enum.TryParse(insertedCoinType, out CoinType coinType);
            var insertedCoin = _coinRepository.GetCoin(coinType);
            var insertCoinCommand = new InsertCoinCommand(insertedCoin);

            var commandResult = (InsertCoinCommandResult)_commandHandler.Handle(insertCoinCommand);
            var jsonResponse = Json(commandResult.DepositedAmount);

            return jsonResponse;
        }

        public ActionResult BuyProduct(string productName, decimal productPrice, int units)
        {
            var product = new ProductDomain(productName, productPrice);

            var buyProductCommand = new BuyProductCommand(product);

            var commandResult = (BuyProductCommandResult)_commandHandler.Handle(buyProductCommand);
            var message = commandResult.Message;
            JsonResult jsonResponse;
            if (!commandResult.Ok)
            {
                jsonResponse = Json(new { success = false, message });

                return jsonResponse;
            }

            var change = MapChange(commandResult.Change);
            units = commandResult.Ok ? units - 1 : units;
            jsonResponse = Json(new { success = true, message, change, units});

            return jsonResponse;
        }

        public ActionResult CancelPurchase()
        {
            var cancelPurchaseCommand = new CancelPurchaseCommand();

            var commandResult = (CancelPurchaseCommandResult)_commandHandler.Handle(cancelPurchaseCommand);
            var change = MapChange(commandResult.Change);
            var jsonResponse = Json(change);

            return jsonResponse;
        }

        private IEnumerable<ProductViewModel> MapProducts(IEnumerable<ProductDomain> products)
        {
            var mappedProducts = new List<ProductViewModel>();
            var groupedProductsByName = products.GroupBy(x => x.Name);
            foreach (var groupedProductByName in groupedProductsByName)
            {
                var productName = groupedProductByName.Key;
                var productPrice = groupedProductByName.First().Price;
                var units = groupedProductByName.Count();
                var mappedProduct = new ProductViewModel(productName, productPrice, units);
                mappedProducts.Add(mappedProduct);
            }

            return mappedProducts;
        }

        private static string MapChange(IEnumerable<Coin> coins)
        {
            var change = new Change();
            if (coins is null)
            {
                return change.ToString();
            }
            foreach (var coin in coins)
            {
                change.AddCoin(coin.Type);
            }

            return change.ToString();
        }
    }
}