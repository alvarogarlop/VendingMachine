﻿using FluentAssertions;
using Moq;
using System.Collections.Generic;
using VendingMachine.Application.CommandHandlers;
using VendingMachine.Application.CommandResults;
using VendingMachine.Application.Commands;
using VendingMachine.Application.Contracts;
using VendingMachine.Domain.Models;
using Xunit;

namespace VendingMachine.Application.UnitTests.CommandHandlers
{
    public class OrchestratorCommandHandlerTests
    {
        private OrchestratorCommandHandler _sut;

        [Fact]
        public void Given_An_OrchestratorCommandHandler_When_Handling_An_InsertCoinCommand_Then_It_Should_Dispatch_It_Only_To_The_InsertCoinCommandHandler()
        {
            var cancelPurchaseCommandHandlerMock = new Mock<ICommandHandler>();

            cancelPurchaseCommandHandlerMock
                .Setup(x => x.CommandName)
                .Returns("CancelPurchaseCommand");

            var insertCoinCommand = CreateDefaultInsertCoinCommand();
            var insertCoinCommandHandlerMock = new Mock<ICommandHandler>();
            insertCoinCommandHandlerMock
                .Setup(x => x.CommandName)
                .Returns("InsertCoinCommand");

            var commandHandlers =
                new List<ICommandHandler>
                {
                    cancelPurchaseCommandHandlerMock.Object,
                    insertCoinCommandHandlerMock.Object
                };

            _sut = new OrchestratorCommandHandler(commandHandlers);
            _sut.Handle(insertCoinCommand);

            cancelPurchaseCommandHandlerMock.Verify(x => x.Handle(insertCoinCommand), Times.Never);
            insertCoinCommandHandlerMock.Verify(x => x.Handle(insertCoinCommand), Times.Once);
        }

        [Fact]
        public void Given_An_OrchestratorCommandHandler_When_Handling_An_InsertCoinCommand_Then_It_Should_Return_An_InsertCoinCommandResult()
        {
            var insertCoinCommand = CreateDefaultInsertCoinCommand();
            var insertCoinCommandHandlerMock = new Mock<ICommandHandler>();
            insertCoinCommandHandlerMock
                .Setup(x => x.CommandName)
                .Returns("InsertCoinCommand");

            var insertCoinCommandResult = new InsertCoinCommandResult(0.10m);
            insertCoinCommandHandlerMock
                .Setup(x => x.Handle(insertCoinCommand))
                .Returns(insertCoinCommandResult);

            var commandHandlers = 
                new List<ICommandHandler>
            {
                insertCoinCommandHandlerMock.Object
            };

            _sut = new OrchestratorCommandHandler(commandHandlers);
            var commandResult = _sut.Handle(insertCoinCommand);

            commandResult.Should().Be(insertCoinCommandResult);
        }

        private InsertCoinCommand CreateDefaultInsertCoinCommand()
        {
            var coin = new Coin(1, CoinType.OneEuro);
            return new InsertCoinCommand(coin);
        }
    }
}