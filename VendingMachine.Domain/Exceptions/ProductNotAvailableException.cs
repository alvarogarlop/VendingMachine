﻿using System;

namespace VendingMachine.Domain.Exceptions
{
    public class ProductNotAvailableException
        : Exception
    {
    }
}