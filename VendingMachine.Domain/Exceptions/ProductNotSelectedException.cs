﻿using System;

namespace VendingMachine.Domain.Exceptions
{
    public class ProductNotSelectedException
        : Exception
    {
    }
}