﻿using System;

namespace VendingMachine.Domain.Exceptions
{
    public class ProductIsGoingToBeDispensedException
        : Exception
    {
    }
}