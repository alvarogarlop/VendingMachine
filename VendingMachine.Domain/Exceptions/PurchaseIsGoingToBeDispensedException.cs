﻿using System;

namespace VendingMachine.Domain.Exceptions
{
    public class PurchaseIsGoingToBeDispensedException
        : Exception
    {
    }
}