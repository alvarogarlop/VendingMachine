﻿using System;

namespace VendingMachine.Domain.Exceptions
{
    public class InsufficientAmountToBuyProductException
        : Exception
    {
    }
}