﻿using System;

namespace VendingMachine.Domain.Exceptions
{
    public class ChangeIsGoingToBeDispensedException
        : Exception
    {
    }
}