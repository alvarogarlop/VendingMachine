﻿namespace VendingMachine.Domain.Models
{
    public enum CoinType
    {
        Undefined = 0,
        OneCent = 1,
        TwoCents = 2,
        FiveCents = 3,
        TenCents = 4,
        TwentyCents = 5,
        FiftyCents = 6,
        OneEuro = 7,
        TwoEuros = 8
    }
}