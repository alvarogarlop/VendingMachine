﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.States;

namespace VendingMachine.Domain.Models
{
    public class VendingMachine
    {
        public decimal InsertedAmount { get; private set; }
        public List<Coin> AvailableCoins { get; }
        public List<Coin> InsertedCoins { get; private set; }
        public List<Product> AvailableProducts { get; }
        public IVendingMachineState State { get; private set; }

        public VendingMachine(List<Coin> availableCoins, List<Product> availableProducts)
        {
            AvailableCoins = availableCoins;
            AvailableProducts = availableProducts;
            InsertedCoins = new List<Coin>();
            InsertedAmount = 0;
            State = new NoCreditState(this);
        }

        public decimal InsertCoin(Coin coin)
        {
            State.InsertCoin(coin);
            return InsertedAmount;
        }

        public IEnumerable<Coin> Buy(Product product, IChangeCalculatorDomainService changeCalculator)
        {
            State.SelectProduct(product);
            State.DispenseProduct(product.Name);
            var change = State.DispenseChange(product.Price, changeCalculator);

            return change;
        }

        public IEnumerable<Coin> CancelPurchase()
        {
            var change = State.CancelPurchase();

            return change;
        }

        public void RemoveCoins(IEnumerable<Coin> coinsToRemove)
        {
            foreach (var coin in coinsToRemove)
            {
                AvailableCoins.Remove(coin);
            }
        }

        public void RemoveProduct(string productName)
        {
            var productToRemove = AvailableProducts.First(x => x.Name.Equals(productName));
            AvailableProducts.Remove(productToRemove);
        }

        public void ResetAmount()
        {
            InsertedAmount = 0.00m;
            InsertedCoins = new List<Coin>();
        }

        public void AddCoin(Coin coin)
        {
            InsertedAmount += coin.Amount;
            AvailableCoins.Add(coin);
            InsertedCoins.Add(coin);
        }

        public bool ProductIsAvailable(string productName)
        {
            return AvailableProducts.Any(x => x.Name.Equals(productName));
        }

        public void UpdateState(IVendingMachineState state)
        {
            State = state;
        }
    }
}