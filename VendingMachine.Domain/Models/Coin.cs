﻿namespace VendingMachine.Domain.Models
{
    public class Coin
    {
        public decimal Amount { get; }
        public CoinType Type { get; }

        public Coin(decimal amount, CoinType coinType)
        {
            Amount = amount;
            Type = coinType;
        }
    }
}