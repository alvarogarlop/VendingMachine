﻿using System.Collections.Generic;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Exceptions;
using VendingMachine.Domain.Models;
using VendingMachineModel = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Domain.States
{
    public class NoCreditState
        : IVendingMachineState
    {
        private readonly VendingMachineModel _vendingMachine;

        public NoCreditState(VendingMachineModel vendingMachine)
        {
            _vendingMachine = vendingMachine;
            _vendingMachine.ResetAmount();
        }

        public void InsertCoin(Coin coin)
        {
            _vendingMachine.AddCoin(coin);

            var availableCreditState = new CreditAvailableState(_vendingMachine);
            _vendingMachine.UpdateState(availableCreditState);
        }

        public void SelectProduct(Product product)
        {
            throw new InsufficientAmountToBuyProductException();
        }

        public IEnumerable<Coin> CancelPurchase()
        {
            return new List<Coin>();
        }

        public void DispenseProduct(string productName)
        {
            throw new ProductNotSelectedException();
        }

        public IEnumerable<Coin> DispenseChange(decimal productPrice, IChangeCalculatorDomainService changeCalculator)
        {
            throw new ProductNotSelectedException();
        }
    }
}