﻿using System.Collections.Generic;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Exceptions;
using VendingMachine.Domain.Models;
using VendingMachineModel = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Domain.States
{
    public class ReturnChangeState
        : IVendingMachineState
    {
        private readonly VendingMachineModel _vendingMachine;

        public ReturnChangeState(VendingMachineModel vendingMachine)
        {
            _vendingMachine = vendingMachine;
        }

        public void InsertCoin(Coin coin)
        {
            throw new ChangeIsGoingToBeDispensedException();
        }

        public void SelectProduct(Product product)
        {
            throw new ChangeIsGoingToBeDispensedException();
        }

        public IEnumerable<Coin> CancelPurchase()
        {
            throw new ChangeIsGoingToBeDispensedException();
        }

        public void DispenseProduct(string productName)
        {
            throw new ChangeIsGoingToBeDispensedException();
        }

        public IEnumerable<Coin> DispenseChange(decimal productPrice, IChangeCalculatorDomainService changeCalculator)
        {
            var change = changeCalculator.CalculateChange(_vendingMachine.InsertedAmount, productPrice, _vendingMachine.AvailableCoins);
            _vendingMachine.RemoveCoins(change);

            var noCreditState = new NoCreditState(_vendingMachine);
            _vendingMachine.UpdateState(noCreditState);

            return change;
        }
    }
}