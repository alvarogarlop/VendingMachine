﻿using System.Collections.Generic;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Exceptions;
using VendingMachine.Domain.Models;
using VendingMachineModel = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Domain.States
{
    public class CreditAvailableState
        : IVendingMachineState
    {
        private readonly VendingMachineModel _vendingMachine;

        public CreditAvailableState(VendingMachineModel vendingMachine)
        {
            _vendingMachine = vendingMachine;
        }

        public void InsertCoin(Coin coin)
        {
            _vendingMachine.AddCoin(coin);
        }

        public void SelectProduct(Product product)
        {
            var notEnoughCredit = !(product.Price <= _vendingMachine.InsertedAmount);
            if (notEnoughCredit)
            {
                throw new InsufficientAmountToBuyProductException();
            }

            var productSoldState = new ProductSoldState(_vendingMachine);
            _vendingMachine.UpdateState(productSoldState);
        }

        public IEnumerable<Coin> CancelPurchase()
        {
            var change = _vendingMachine.InsertedCoins;
            _vendingMachine.RemoveCoins(_vendingMachine.InsertedCoins);
            _vendingMachine.ResetAmount();

            var noCreditState = new NoCreditState(_vendingMachine);
            _vendingMachine.UpdateState(noCreditState);

            return change;
        }

        public void DispenseProduct(string productName)
        {
            throw new ProductNotSelectedException();
        }

        public IEnumerable<Coin> DispenseChange(decimal productPrice, IChangeCalculatorDomainService changeCalculator)
        {
            throw new ProductNotSelectedException();
        }
    }
}