﻿using System.Collections.Generic;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Exceptions;
using VendingMachine.Domain.Models;
using VendingMachineModel = VendingMachine.Domain.Models.VendingMachine;

namespace VendingMachine.Domain.States
{
    public class ProductSoldState
        : IVendingMachineState
    {
        private readonly VendingMachineModel _vendingMachine;

        public ProductSoldState(VendingMachineModel vendingMachine)
        {
            _vendingMachine = vendingMachine;
        }

        public void InsertCoin(Coin coin)
        {
            throw new ProductIsGoingToBeDispensedException();
        }

        public void SelectProduct(Product product)
        {
            throw new ProductIsGoingToBeDispensedException();
        }

        public IEnumerable<Coin> CancelPurchase()
        {
            throw new ProductIsGoingToBeDispensedException();
        }

        public void DispenseProduct(string productName)
        {
            var productIsNotAvailable = !_vendingMachine.ProductIsAvailable(productName);
            if (productIsNotAvailable)
            {
                var availableCreditState = new CreditAvailableState(_vendingMachine);
                _vendingMachine.UpdateState(availableCreditState);

                throw new ProductNotAvailableException();
            }
            _vendingMachine.RemoveProduct(productName);
            var returnChangeState = new ReturnChangeState(_vendingMachine);
            _vendingMachine.UpdateState(returnChangeState);
        }

        public IEnumerable<Coin> DispenseChange(decimal productPrice, IChangeCalculatorDomainService changeCalculator)
        {
            throw new ProductIsGoingToBeDispensedException();
        }
    }
}