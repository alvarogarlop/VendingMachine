﻿using System.Collections.Generic;
using VendingMachine.Domain.Models;

namespace VendingMachine.Domain.Contracts
{
    public interface IChangeCalculatorDomainService
    {
        List<Coin> CalculateChange(decimal paidAmount, decimal price, List<Coin> availableCoins);
    }
}