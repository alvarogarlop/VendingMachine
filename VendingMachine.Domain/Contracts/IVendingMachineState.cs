﻿using System.Collections.Generic;
using VendingMachine.Domain.Models;

namespace VendingMachine.Domain.Contracts
{
    public interface IVendingMachineState
    {
        void InsertCoin(Coin coin);
        void SelectProduct(Product product);
        IEnumerable<Coin> CancelPurchase();
        void DispenseProduct(string productName);
        IEnumerable<Coin> DispenseChange(decimal productPrice, IChangeCalculatorDomainService changeCalculator);
    }
}