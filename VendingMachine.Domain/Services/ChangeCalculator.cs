﻿using System.Collections.Generic;
using System.Linq;
using VendingMachine.Domain.Contracts;
using VendingMachine.Domain.Models;

namespace VendingMachine.Domain.Services
{
    public class ChangeCalculator
        : IChangeCalculatorDomainService
    {
        public List<Coin> CalculateChange(decimal paidAmount, decimal price, List<Coin> availableCoins)
        {
            var change = new List<Coin>();
            var changeAmount = paidAmount - price;

            var sortedCoins = availableCoins.OrderByDescending(x => x.Amount);
            foreach (var coin in sortedCoins)
            {
                if (coin.Amount > changeAmount)
                {
                    continue;
                }

                var unitsOfCoin = (int)(changeAmount / coin.Amount);
                var calculatedCoins = Enumerable.Repeat(coin, unitsOfCoin);
                change.AddRange(calculatedCoins);

                changeAmount -= coin.Amount * unitsOfCoin;
                if (changeAmount == 0)
                {
                    break;
                }
            }

            return change;
        }
    }
}